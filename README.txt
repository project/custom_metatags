CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * More information

INTRODUCTION
------------

The Custom Metatags allow to create custom metatags as you want, provide a
input text in the advanced option in the content to create it.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install as usual, see http://drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

To enable Custom Metatags, go to Structure > Content types > Manage Form 
Display

Enable the field "Custom Metags" for your content.


MORE INFORMATION
----------------

 *  https://www.drupal.org/project/custom_metatags
